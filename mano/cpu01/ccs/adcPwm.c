
// Included Files
#include "F28x_Project.h"
#include "stdlib.h"
#include "math.h"

//Definitions

#define EPWM2_TIMER_TBPRD  2000  // Period register
#define EPWM2_MAX_CMPA     4096
#define EPWM2_MIN_CMPA        1
#define EPWM2_MAX_CMPB     4096
#define EPWM2_MIN_CMPB        1
//
//#define EPWM3_TIMER_TBPRD  2000  // Period register
//#define EPWM3_MAX_CMPA     2000
//#define EPWM3_MIN_CMPA        1
//#define EPWM3_MAX_CMPB     2000
//#define EPWM3_MIN_CMPB        1
//
#define EPWM_CMP_UP           1
#define EPWM_CMP_DOWN         0
//
//// Global variables

int16 convert1=0;
int16 convert2=0;
int16 Mcurrent1=0;
int16 Mcurrent2=0;
int32 MuscleS=0;
long  error1=0;
long  error2=0;
long  state1=0;
long  state2=0;
long valueA=0;
int estado=0;
char *manual;
char *msg;

typedef struct
{
    volatile struct EPWM_REGS *EPwmRegHandle;
    Uint16 EPwm_CMPA_Direction;
    Uint16 EPwm_CMPB_Direction;
    Uint16 EPwmTimerIntCount;
    Uint16 EPwmMaxCMPA;
    Uint16 EPwmMinCMPA;
    Uint16 EPwmMaxCMPB;
    Uint16 EPwmMinCMPB;
}EPWM_INFO;


EPWM_INFO epwm2_info;
//EPWM_INFO epwm3_info;

// Define functions in program
void delay_loop(void);
void Gpio_select(void);
void ConfigureADC(void);
void SetupADCContinuous();
void InitEPwm2(void);
//void InitEPwm3(void);
//void scia_echoback_init(void);
//void scia_fifo_init(void);
//void scia_xmit(int a);
//void scia_msg(char *msg);
//void control(int modo);
//void controlM1(int error);
//void controlM2(int error);

//Define interrupts in program
__interrupt void epwm2_isr(void);
//__interrupt void epwm3_isr(void);

//Connections

// ADC0 = ADCINA0 Motor current 2
// ADC1 = ADCINA1 Motor current 1
// ADC2 = ADCINA2 Muscle sensor
// EPWM2A = GPIO2 Motor2 PWM
// EPWM2B = GPIO3 Not used
// EPWM2A = GPIO4 Motor1 PWM
// EPWM2B = GPIO5 Not used
// RX     = GPI42-->Tx(Bluetooth)
// TX     = GPI43-->Rx(Bluetooth)
// Spin control 1A = GPIO12
// Spin control 1B = GPIO13
// Spin control 2A = GPIO14
// Spin control 2B = GPIO15

// Main
void main(void)
{
//   int contador=0;
//   int j=0;
//   Uint16 ReceivedChar;
//   estado = 0;

   InitSysCtrl();

   CpuSysRegs.PCLKCR2.bit.EPWM2=1; // enable PWM2
//   CpuSysRegs.PCLKCR2.bit.EPWM3=1; // enable PWM2

   DINT;
   InitPieCtrl();
   IER = 0x0000;
   IFR = 0x0000;
   InitPieVectTable();
   //
   // Interrupts that are used in this example are re-mapped to
   // ISR functions found within this file.
   //
   EALLOW; // This is needed to write to EALLOW protected registers
   PieVectTable.EPWM2_INT = &epwm2_isr;
//   PieVectTable.EPWM3_INT = &epwm3_isr;
   EDIS;   // This is needed to disable write to EALLOW protected registers

   //
   // For this example, only initialize the ePWM
   //
   EALLOW;
   CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 0;
   EDIS;
//
   InitEPwm2();
////   InitEPwm3();
//
   EALLOW;
   CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 1;
   EDIS;
////
//// Enable CPU INT3 which is connected to EPWM1-3 INT:
////
   IER |= M_INT3;
//
////
//// Enable EPWM INTn in the PIE: Group 3 interrupt 1-3
////
   PieCtrlRegs.PIEIER3.bit.INTx1 = 1;
   PieCtrlRegs.PIEIER3.bit.INTx2 = 1;
   PieCtrlRegs.PIEIER3.bit.INTx3 = 1;

//
// Enable global Interrupts and higher priority real-time debug events:
//
   EINT;  // Enable Global interrupt INTM
   ERTM;  // Enable Global real time interrupt DBGM

   Gpio_select();
   ConfigureADC();
   SetupADCContinuous();

   InitEPwm2Gpio();     //Initialize GPIO pins for ePWM2
//   InitEPwm3Gpio();     //Initialize GPIO pins for ePWM2
//   GPIO_SetupPinMux(43, GPIO_MUX_CPU1, 15); //28 RX //43 pin9
//   GPIO_SetupPinOptions(43, GPIO_INPUT, GPIO_PUSHPULL);  //GPIO_INVERT);  // GPIO_PUSHPULL);
//   GPIO_SetupPinMux(42, GPIO_MUX_CPU1, 15);   //29 TX //42 pin10
//   GPIO_SetupPinOptions(42, GPIO_OUTPUT, GPIO_ASYNC);   // GPIO_INVERT);   //GPIO_ASYNC);
//
//   scia_fifo_init();       // Initialize the SCI FIFO
//   scia_echoback_init();   // Initialize SCI for echoback

   //
   // Enable EPWM INTn in the PIE: Group 3 interrupt 1-3
   //
//   PieCtrlRegs.PIEIER3.bit.INTx1 = 1;
//   PieCtrlRegs.PIEIER3.bit.INTx2 = 1;
//   PieCtrlRegs.PIEIER3.bit.INTx3 = 1;

//   EINT;  // Enable Global interrupt INTM
//   ERTM;

   //Initialize on zero GPIO for spin control
//   GpioDataRegs.GPADAT.bit.GPIO12=0;  //G 1
//   DELAY_US(10);
//   GpioDataRegs.GPADAT.bit.GPIO13=0;  //G 1
//   DELAY_US(10);
//   GpioDataRegs.GPADAT.bit.GPIO14=0;  //G 1
//   DELAY_US(10);
//   GpioDataRegs.GPADAT.bit.GPIO15=0;  //G 1
//   DELAY_US(10);

   //Initialize PWM�s in lowest possible value
   EPwm2Regs.CMPA.bit.CMPA = 4096;   // Set compare A value EPWM2A = GPIO2
   EPwm2Regs.CMPB.bit.CMPB = 4096;   // Set Compare B value EPWM2B = GPIO3
//   EPwm3Regs.CMPA.bit.CMPA = 2000;   // Set compare A value EPWM2A = GPIO4
//   EPwm3Regs.CMPB.bit.CMPB = 2000;    // Set Compare B value EPWM2B = GPIO5 ??

   for(;;)
   {
       AdcaRegs.ADCSOCFRC1.bit.SOC0 = 1;   //Start conversion of channel 0 = ADCINA0
       DELAY_US(10);
       Mcurrent1 = AdcaResultRegs.ADCRESULT0;
       AdcaRegs.ADCINTSEL1N2.bit.INT1E = 0; //Stop sampling
       EPwm2Regs.CMPA.bit.CMPA = Mcurrent1;    // Set compare A value EPWM2A = GPIO4
       DELAY_US(5);
       EPwm2Regs.CMPB.bit.CMPB = Mcurrent1;    // Set Compare B value EPWM2B = GPIO5 ??
//      while(SciaRegs.SCIFFRX.bit.RXFFST == 0)//Wait for the user to write a character
//      {
//         control(estado);
//      }
//
//      ReceivedChar = SciaRegs.SCIRXBUF.all;
//      //scia_xmit(ReceivedChar);  //Function to send character
//       ltoa(Mcurrent1,msg);
//       scia_msg(msg);
//       scia_xmit(13);//Function to send string of characters
//      if (ReceivedChar == 65)     //If character = 'A'
//      {
//        estado=0;
//      }
//      else if (ReceivedChar == 77) //If character = 'M'
//      {
//        estado=1;
//      }
//      else
//      {
//          if(ReceivedChar == 68)
//          {
//              convert1=atoi(manual);
//              for(j=0;j<=contador;j++)
//              {
//                  manual[j]= 0;
//              }
//              contador=0;
//          }
//          else if(ReceivedChar == 73)
//          {
//              convert2=atoi(manual);
//              for(j=0;j<=contador;j++)
//              {
//                  manual[j]= 0;
//              }
//              contador=0;
//          }
//          else
//          {
//             manual[contador]=ReceivedChar;
//             contador++;
//          }
//      }
   }
}

void delay_loop()
{
    short i;
    for (i = 0; i < 1000; i++) {DELAY_US(200);}
}


void Gpio_select(void)
{
    EALLOW;
    GpioCtrlRegs.GPAMUX1.all = 0x00000000;  // All GPIO
    GpioCtrlRegs.GPAMUX2.all = 0x00000000;  // All GPIO
    GpioCtrlRegs.GPBMUX1.all = 0x00000000;  // All GPIO
    GpioCtrlRegs.GPADIR.all  = 0xFFFFFFFF;   // All outputs 1 indicates output ,0 indicate input
    GpioCtrlRegs.GPBDIR.all  = 0x00001FFF;   // All outputs
    EDIS;
}
void ConfigureADC(void)
{
    EALLOW;

    //
    //write configurations
    //
    AdcaRegs.ADCCTL2.bit.PRESCALE = 6; //set ADCCLK divider to /4
    AdcSetMode(ADC_ADCA, ADC_RESOLUTION_12BIT, ADC_SIGNALMODE_SINGLE);

    //
    //Set pulse positions to late
    //
    AdcaRegs.ADCCTL1.bit.INTPULSEPOS = 1;

    //
    //power up the ADC
    //
    AdcaRegs.ADCCTL1.bit.ADCPWDNZ = 1;

    //
    //delay for 1ms to allow ADC time to power up
    //
    DELAY_US(1000);

    EDIS;
}

//
// SetupADCContinuous - setup the ADC to continuously convert on one channel
//
void SetupADCContinuous(void)
{
    Uint16 acqps;

    //
    //determine minimum acquisition window (in SYSCLKS) based on resolution
    //
    if(ADC_RESOLUTION_12BIT == AdcaRegs.ADCCTL2.bit.RESOLUTION)
    {
        acqps = 14; //75ns
    }
    else //resolution is 16-bit
    {
        acqps = 63; //320ns
    }

    EALLOW;
    AdcaRegs.ADCSOC0CTL.bit.CHSEL  = 0;        //Converts analog input from channel 0 = ADCINA0
    AdcaRegs.ADCSOC1CTL.bit.CHSEL  = 1;        //Converts analog input from channel 1 = ADCINA1
    AdcaRegs.ADCSOC2CTL.bit.CHSEL  = 2;        //Converts analog input from channel 2 = ADCINA2

    AdcaRegs.ADCSOC0CTL.bit.ACQPS  = acqps;    //Determines sample time for channel 0
    AdcaRegs.ADCSOC1CTL.bit.ACQPS  = acqps;    //Determines sample time for channel 1
    AdcaRegs.ADCSOC2CTL.bit.ACQPS  = acqps;    //Determines sample time for channel 2

    AdcaRegs.ADCINTSEL1N2.bit.INT1E = 0;       //Disable INT1 flag (Internal ADC interrupt)
    AdcaRegs.ADCINTSEL1N2.bit.INT1CONT = 0;    //No pulses are generated until ADCINT1 flag is cleared by user
    AdcaRegs.ADCINTSEL1N2.bit.INT1SEL = 2;     //End of conversion 1 (EOC1) will set INT1 flag

    //
    //ADCINT2 will trigger first 2 SOCs
    //
    AdcaRegs.ADCINTSOCSEL1.bit.SOC0 = 1;       //ADCINT1 will trigger SOC0(Start of conversion 0)
    AdcaRegs.ADCINTSOCSEL1.bit.SOC1 = 1;       //ADCINT1 will trigger SOC1(Start of conversion 1)
    AdcaRegs.ADCINTSOCSEL1.bit.SOC2 = 1;       //ADCINT1 will trigger SOC1(Start of conversion 2)
    EDIS;
}

//
// InitEPwm2Example - Initialize EPWM2 configuration
//
void InitEPwm2()
{
    //
    // Setup TBCLK
    //
    EPwm2Regs.TBPRD = EPWM2_TIMER_TBPRD;         // Set timer period 801 TBCLKs
    EPwm2Regs.TBPHS.bit.TBPHS = 0x0000;          // Phase is 0
    EPwm2Regs.TBCTR = 0x0000;                    // Clear counter

    //
    // Set Compare values
    //
    EPwm2Regs.CMPA.bit.CMPA = EPWM2_MIN_CMPA;    // Set compare A value
    EPwm2Regs.CMPB.bit.CMPB = EPWM2_MIN_CMPB;    // Set Compare B value

    //
    // Setup counter mode
    //
    EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Count up and down
    EPwm2Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
    EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;       // Clock ratio to SYSCLKOUT
    EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;

    //
    // Setup shadowing
    //
    EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
    EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO; // Load on Zero
    EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

    //
    // Set actions
    //
    EPwm2Regs.AQCTLA.bit.CAU = AQ_SET;         // Set PWM2A on event A, up
                                               // count
    EPwm2Regs.AQCTLA.bit.CBD = AQ_CLEAR;       // Clear PWM2A on event B, down
                                               // count

    EPwm2Regs.AQCTLB.bit.CAU = AQ_SET;         // Set PWM2A on event A, up
                                               // count
    EPwm2Regs.AQCTLB.bit.CBD = AQ_CLEAR;       // Clear PWM2A on event B, down
                                               // count

    //
    // Interrupt where we will change the Compare Values
    //
    EPwm2Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;    // Select INT on Zero event
    EPwm2Regs.ETSEL.bit.INTEN = 1;               // Enable INT
    EPwm2Regs.ETPS.bit.INTPRD = ET_3RD;          // Generate INT on 3rd event

    //
    // Information this example uses to keep track
    // of the direction the CMPA/CMPB values are
    // moving, the min and max allowed values and
    // a pointer to the correct ePWM registers
    //
    epwm2_info.EPwm_CMPA_Direction = EPWM_CMP_UP;  // Start by increasing CMPA
    epwm2_info.EPwm_CMPB_Direction = EPWM_CMP_UP;  // & increasing CMPB
    epwm2_info.EPwmTimerIntCount = 0;              // Zero the interrupt counter
    epwm2_info.EPwmRegHandle = &EPwm2Regs;         // Set the pointer to the
                                                   // ePWM module
    epwm2_info.EPwmMaxCMPA = EPWM2_MAX_CMPA;       // Setup min/max CMPA/CMPB
                                                   // values
    epwm2_info.EPwmMinCMPA = EPWM2_MIN_CMPA;
    epwm2_info.EPwmMaxCMPB = EPWM2_MAX_CMPB;
    epwm2_info.EPwmMinCMPB = EPWM2_MIN_CMPB;
}
//
// epwm2_isr - EPWM2 ISR
//
__interrupt void epwm2_isr(void)
{
    //
    // Clear INT flag for this timer
    //
    EPwm2Regs.ETCLR.bit.INT = 1;

    //
    // Acknowledge this interrupt to receive more interrupts from group 3
    //
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}
//void InitEPwm3()
//{
//    //
//    // Setup TBCLK
//    //
//    EPwm3Regs.TBPRD = EPWM3_TIMER_TBPRD;         // Set timer period 801 TBCLKs
//    EPwm3Regs.TBPHS.bit.TBPHS = 0x0000;          // Phase is 0
//    EPwm3Regs.TBCTR = 0x0000;                    // Clear counter
//
//    //
//    // Set Compare values
//    //
//    EPwm3Regs.CMPA.bit.CMPA = EPWM3_MIN_CMPA;    // Set compare A value
//    EPwm3Regs.CMPB.bit.CMPB = EPWM3_MIN_CMPB;    // Set Compare B value
//
//    //
//    // Setup counter mode
//    //
//    EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Count up and down
//    EPwm3Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
//    EPwm3Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;       // Clock ratio to SYSCLKOUT
//    EPwm3Regs.TBCTL.bit.CLKDIV = TB_DIV1;
//
//    //
//    // Setup shadowing
//    //
//    EPwm3Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
//    EPwm3Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
//    EPwm3Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO; // Load on Zero
//    EPwm3Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;
//
//    //
//    // Set actions
//    //
//    EPwm3Regs.AQCTLA.bit.CAU = AQ_SET;         // Set PWM2A on event A, up
//                                               // count
//    EPwm3Regs.AQCTLA.bit.CBD = AQ_CLEAR;       // Clear PWM2A on event B, down
//                                               // count
//
//    EPwm3Regs.AQCTLB.bit.CAU = AQ_SET;         // Set PWM2A on event A, up
//                                               // count
//    EPwm3Regs.AQCTLB.bit.CBD = AQ_CLEAR;       // Clear PWM2A on event B, down
//                                               // count
//
//    //
//    // Interrupt where we will change the Compare Values
//    //
//    EPwm3Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;    // Select INT on Zero event
//    EPwm3Regs.ETSEL.bit.INTEN = 1;               // Enable INT
//    EPwm3Regs.ETPS.bit.INTPRD = ET_3RD;          // Generate INT on 3rd event
//
//    //
//    // Information this example uses to keep track
//    // of the direction the CMPA/CMPB values are
//    // moving, the min and max allowed values and
//    // a pointer to the correct ePWM registers
//    //
//    epwm3_info.EPwm_CMPA_Direction = EPWM_CMP_UP;  // Start by increasing CMPA
//    epwm3_info.EPwm_CMPB_Direction = EPWM_CMP_UP;  // & increasing CMPB
//    epwm3_info.EPwmTimerIntCount = 0;              // Zero the interrupt counter
//    epwm3_info.EPwmRegHandle = &EPwm3Regs;         // Set the pointer to the
//                                                   // ePWM module
//    epwm3_info.EPwmMaxCMPA = EPWM3_MAX_CMPA;       // Setup min/max CMPA/CMPB
//                                                   // values
//    epwm3_info.EPwmMinCMPA = EPWM3_MIN_CMPA;
//    epwm3_info.EPwmMaxCMPB = EPWM3_MAX_CMPB;
//    epwm3_info.EPwmMinCMPB = EPWM3_MIN_CMPB;
//}
////
//// epwm2_isr - EPWM2 ISR
////
//__interrupt void epwm3_isr(void)
//{
//    //
//    // Clear INT flag for this timer
//    //
//    EPwm3Regs.ETCLR.bit.INT = 1;
//
//    //
//    // Acknowledge this interrupt to receive more interrupts from group 3
//    //
//    PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
//}
////
////  scia_echoback_init - Test 1,SCIA  DLB, 8-bit word, baud rate 0x000F,
////                       default, 1 STOP bit, no parity
////
//void scia_echoback_init()
//{
//    //
//    // Note: Clocks were turned on to the SCIA peripheral
//    // in the InitSysCtrl() function
//    //
//
//    SciaRegs.SCICCR.all = 0x0007;   // 1 stop bit,  No loopback
//                                    // No parity,8 char bits,
//                                    // async mode, idle-line protocol
//    SciaRegs.SCICTL1.all = 0x0003;  // enable TX, RX, internal SCICLK,
//                                    // Disable RX ERR, SLEEP, TXWAKE
//    SciaRegs.SCICTL2.all = 0x0003;
//    SciaRegs.SCICTL2.bit.TXINTENA = 1;
//    SciaRegs.SCICTL2.bit.RXBKINTENA = 1;
//
//    //
//    // SCIA at 9600 baud
//    // @LSPCLK = 50 MHz (200 MHz SYSCLK) HBAUD = 0x02 and LBAUD = 0x8B.  BRR=LSPCLK/76799=651.0
//    // @LSPCLK = 30 MHz (120 MHz SYSCLK) HBAUD = 0x01 and LBAUD = 0x86.  BRR=LSPCLK/76799=390.63
//    // @LSPCLK = 25 MHz (100 MHz SYSCLK) HBAUD = 0x01 and LBAUD = 0x45.  BRR=LSPCLK/76799=325.52
//    //
//    SciaRegs.SCIHBAUD.all = 0x0001;     //0x0001
//    SciaRegs.SCILBAUD.all = 0x0045;     //0x0045
//
//    SciaRegs.SCICTL1.all = 0x0023;    // Relinquish SCI from Reset
//}
////
//// scia_xmit - Transmit a character from the SCI
////
//void scia_xmit(int a)
//{
//    while (SciaRegs.SCIFFTX.bit.TXFFST != 0) {}
//    SciaRegs.SCITXBUF.all =a;
//}
//
////
//// scia_msg - Transmit message via SCIA
////
//void scia_msg(char * msg)
//{
//    int i;
//    i = 0;
//    while(msg[i] != '\0')
//    {
//        scia_xmit(msg[i]);
//        i++;
//    }
//}
////
//// scia_fifo_init - Initialize the SCI FIFO
////
//void scia_fifo_init()
//{
//    SciaRegs.SCIFFTX.all = 0xE040;
//    SciaRegs.SCIFFRX.all = 0x2044;
//    SciaRegs.SCIFFCT.all = 0x0;
//}
//void control(int modo)
//{
//    if (modo == 0)
//    {
//        AdcaRegs.ADCSOCFRC1.bit.SOC0 = 1;   //Start conversion of channel 0 = ADCINA0
//        AdcaRegs.ADCSOCFRC1.bit.SOC1 = 1;   //Start conversion of channel 1 = ADCINA1
//        AdcaRegs.ADCSOCFRC1.bit.SOC2 = 1;   //Start conversion of channel 2 = ADCINA2
//        DELAY_US(10);
//
//        Mcurrent1 = AdcaResultRegs.ADCRESULT1;
//        Mcurrent2 = AdcaResultRegs.ADCRESULT0;
//        MuscleS   = (AdcaResultRegs.ADCRESULT2);
//        if(AdcaResultRegs.ADCRESULT2 != 0)
//        {
//            MuscleS   = AdcaResultRegs.ADCRESULT2;
//        }
//        DELAY_US(10);
//
//        valueA=(MuscleS*170)/4095;
//        AdcaRegs.ADCINTSEL1N2.bit.INT1E = 0; //Stop sampling
//
//        error1 = valueA - Mcurrent1;
//        error2 = valueA - Mcurrent2;
//
//        controlM1(error1); //Function to control torque in motor 1
//        controlM2(error2); //Function to control torque in motor 1
//
//    }
//    else
//    {
//        AdcaRegs.ADCSOCFRC1.bit.SOC0 = 1;   //Start conversion of channel 0 = ADCINA0
//        AdcaRegs.ADCSOCFRC1.bit.SOC1 = 1;   //Start conversion of channel 1 = ADCINA1
//        DELAY_US(10);
//
//        Mcurrent1 = AdcaResultRegs.ADCRESULT1;
//        Mcurrent2 = AdcaResultRegs.ADCRESULT0;
//
//        AdcaRegs.ADCINTSEL1N2.bit.INT1E = 0; //Stop sampling
//
//        error1 = convert1 - Mcurrent1;
//        error2 = convert2 - Mcurrent2;
//
//        controlM1(error1); //Function to control torque in motor 1
//        controlM2(error2); //Function to control torque in motor 1
//     }
//}
//void controlM1(int error)
//{
//
//    //Initialize on zero GPIO for spin control
//    if (error>=1)
//    {
//        GpioDataRegs.GPADAT.bit.GPIO12=1;  //G 1
//        DELAY_US(10);
//        GpioDataRegs.GPADAT.bit.GPIO13=0;  //G 1
//        DELAY_US(10);
//        state1=EPwm3Regs.CMPA.bit.CMPA;
//        if(state1>EPWM3_MIN_CMPA)
//        {
//            EPwm3Regs.CMPA.bit.CMPA--;    // Set compare A value EPWM2A = GPIO4
//            EPwm3Regs.CMPB.bit.CMPB--;    // Set Compare B value EPWM2B = GPIO5 ??
//        }
//    }
//    else if(error<1)
//    {
//        GpioDataRegs.GPADAT.bit.GPIO12=1;  //G 1
//        DELAY_US(10);
//        GpioDataRegs.GPADAT.bit.GPIO13=0;  //G 1
//        DELAY_US(10);
//        state1=EPwm3Regs.CMPA.bit.CMPA;
//
//        if(state1<EPWM3_MAX_CMPA)
//        {
//            EPwm3Regs.CMPA.bit.CMPA++;    // Set compare A value EPWM2A = GPIO4
//            EPwm3Regs.CMPB.bit.CMPB++;    // Set Compare B value EPWM2B = GPIO5 ??
//        }
//    }
//    else
//    {
//        //Do nothing
//    }
//}
//void controlM2(int error)
//{
//
//    //Initialize on zero GPIO for spin control
//    if (error>=1)
//    {
//        GpioDataRegs.GPADAT.bit.GPIO14=1;  //G 1
//        DELAY_US(10);
//        GpioDataRegs.GPADAT.bit.GPIO15=0;  //G 1
//        DELAY_US(10);
//        state2=EPwm2Regs.CMPA.bit.CMPA;
//        if(state2>EPWM2_MIN_CMPA)
//        {
//            EPwm2Regs.CMPA.bit.CMPA--;    // Set compare A value EPWM2A = GPIO4
//            EPwm2Regs.CMPB.bit.CMPB--;    // Set Compare B value EPWM2B = GPIO5 ??
//        }
//    }
//    else if(error<1)
//    {
//        GpioDataRegs.GPADAT.bit.GPIO14=1;  //G 1
//        DELAY_US(10);
//        GpioDataRegs.GPADAT.bit.GPIO15=0;  //G 1
//        DELAY_US(10);
//        state2=EPwm2Regs.CMPA.bit.CMPA;
//        if(state2<EPWM2_MAX_CMPA)
//        {
//            EPwm2Regs.CMPA.bit.CMPA++;    // Set compare A value EPWM2A = GPIO4
//            EPwm2Regs.CMPB.bit.CMPB++;    // Set Compare B value EPWM2B = GPIO5 ??
//        }
//    }
//    else
//    {
//        //Do nothing
//    }
//}
